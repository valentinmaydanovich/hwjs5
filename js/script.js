function createNewUser() {
    const _firstname = prompt ("What is your first name?");
    const _lastname = prompt ("What is your last name?");
    const newUser = {
        _firstname,
        _lastname,
        getLogin () {
            return((this._firstname[0]+this._lastname).toLowerCase());
        },
            set firstname(value) {
                this._firstname = value;
            },
            get firstname() {
                return this._firstname;
            },
            set lastname(value) {
                this._lastname = value;
            },
            get lastname() {
                return this._lastname;
            },
    }
    return newUser;
}
